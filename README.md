# README #

If you use this code for academic research, you are highly encouraged to cite the following paper by H. M. Tarek Ullah, ZishanAhmed Onik, Riashat Islam, Dip Nandi:

H. M. T. Ullah, Z. Onik, R. Islam and D. Nandi, "Alzheimer's Disease and Dementia Detection from 3D Brain MRI Data Using Deep Convolutional Neural Networks," 2018 3rd International Conference for Convergence in Technology (I2CT), Pune, 2018, pp. 1-3. doi: 10.1109/I2CT.2018.8529808 keywords: {Alzheimer's disease;Magnetic resonance imaging;Three-dimensional displays;Biological neural networks;Neural Networks;Deep Learning;3D Brain MRI;Alzheimer's Disease And Dementia;Machine Learning;Big Data;High Dimensional Input}, URL: http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8529808&isnumber=8529154

### What is this repository for? ###

This is a Deep Learning thesis work.Our data source is http://www.oasis-brains.org/ and we have used cross-sectional data.
we have applied Deep Convolutional Neural Networks and trained our model 545 times using GPU. 
Our target was to detect Alzheimer's Disease and we have gained 80.25% accuracy after 545 epochs.

### How do I get set up? ###

Dependency :

1. Nilearn
2. Tensorflow
3. keras
4. Scikit-learn
5. OpenCV
6. Matplotlib
7. Jupyter Notebook
8. https://www.floydhub.com/

### License & Copyright ###

Copyright (c) 2018 H. M. Tarek Ullah

Licensed under the [MIT License](LICENSE)


